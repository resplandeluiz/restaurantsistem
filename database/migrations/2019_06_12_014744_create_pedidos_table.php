<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePedidosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {


        Schema::create('pedidos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')
                ->references('id')
                ->on('users');

            $table->timestamps();
        });

        Schema::create('pedidos_products', function (Blueprint $table) {

            $table->increments('id');


            $table->integer('pedido_id')->unsigned()->nullable();
            $table->foreign('pedido_id')
                ->references('id')
                ->on('pedidos');

            $table->integer('produto_id')->unsigned()->nullable();
            $table->foreign('produto_id')
                ->references('id')
                ->on('products');





            $table->string('valor_produto');
            $table->string('qtd_produto');

            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pedidos_products');
        Schema::dropIfExists('pedidos');
    }
}
