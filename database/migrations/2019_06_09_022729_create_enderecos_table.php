<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEnderecosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {



        Schema::create('enderecos', function (Blueprint $table) {

            $table->increments('id');
            $table->string('logradouro')->nullable();
            $table->string('bairro')->nullable();
            $table->text('complemento')->nullable();
            $table->string('numero')->nullable();


            $table->string('cep');

            $table->string('estado');

            $table->string('cidade');

            $table->timestamps();

        });




    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::dropIfExists('enderecos');


    }
}
