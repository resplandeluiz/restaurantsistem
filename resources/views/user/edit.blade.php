@extends('layouts.app')

@section('content')

<div class="container">

        <div class="form-group">
            <label for="exampleFormControlInput1">Nome</label>
            <input type="text" class="form-control" id="name" value="{{ auth()->user()->name }}" placeholder="name@example.com">
        </div>
        <div class="form-group">
            <label for="exampleFormControlInput1">Email</label>
            <input type="email" class="form-control" id="email" value="{{ auth()->user()->email }}" placeholder="name@example.com">
        </div>

        <input type="submit" class="btn btn-info" id="updateUser" value="Atualizar">


</div>

@endsection




