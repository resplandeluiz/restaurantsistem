<div class="my-3 p-3 bg-white rounded shadow-sm">
    <div class="col-12 row">

        <h6 class="border-bottom border-gray pb-2 mb-0"> Meus Produtos </h6>

    </div>


    @foreach($products as $product)
        <div class="media text-muted pt-3">

            @if($product->image)
                <img src="{{ asset('storage/'.$product->image)  }}" class="img-fluid" width="50px">
            @else
                <img src="{{ asset('img/product-bg.jpg') }}"
                     class="img-thumbnail rounded float-left" alt="..." width="50px">
            @endif
            <div class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                <div class="d-flex justify-content-between align-items-center w-100">
                    <strong class="text-gray-dark">{{ $product->nome  }}</strong>
                    <a href="#">R$: {{ $product->valor  }}</a>
                    <a href="#">Descrição :{{ $product->desc  }}</a>
                    <button class="updateRecord btn btn-info" data-id="{{ $product->id }}">Editar</button>
                    <button class="deleteRecord btn btn-danger" data-id="{{ $product->id }}">Excluir</button>


                </div>

            </div>
        </div>



    @endforeach


</div>

{{ $products->links() }}

<script>
    $(".deleteRecord").click(function () {
        var id = $(this).data("id");

        var token = $("meta[name='csrf-token']").attr("content");

        Swal.fire({
            title: 'Certeza que deseja excluir ?',
            text: "Essa ação não poderá ser revertida!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Sim, excluir!'
        }).then((result) => {
            if (result.value) {
                $.ajax(
                    {
                        url: "/product/" + id,
                        method: 'delete',
                        data: {
                            "id": id,
                            "_token": token,
                        },
                        success: function (data) {

                            Swal.fire(
                                'Parabéns!',
                                'Produto excluído com sucesso!',
                                'success'
                            );


                        },
                        error: function (error) {
                            console.log(error)
                        }
                    });


            }


        });

    });


    $(".updateRecord").click(function () {

        var id = $(this).data("id");
        var token = $("meta[name='csrf-token']").attr("content");

        $.ajax(
            {
                url: "/product/getProduct/" + id,
                method: 'get',
                data: {
                    "id": id,
                    "_token": token,
                },
                success: function (data) {

                    $('#nome').val(data.nome)
                    $('#desc').val(data.desc)
                    $('#valor').val(data.valor)
                    $('#image').val(data.image)
                    $('#restaurants_id').val(data.restaurants_id)
                    $('#id').val(data.id)
                    $("#image").attr("src","/storage/"+data.image);


                }
            });


    });


</script>

