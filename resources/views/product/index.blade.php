@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">

                    <div class="card mb-3">
                        <div class="row no-gutters">

                            <div class="col-md-12">


                               {{ Form::open(array('id'=>'formProduct','files' => 'true')) }}


                                @csrf
                                <div class="card-body row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            @if( $product->image )
                                                <img src="{{ asset('storage/'. $product->image) }}" id="image"
                                                     class="img-thumbnail rounded float-left" alt="...">
                                            @else
                                                <img src="{{ asset('img/product-bg.jpg') }}" id="image"
                                                     class="img-thumbnail rounded float-left" alt="...">
                                            @endif

                                            <input type="hidden" name="restaurants_id" value="{{$id}}">
                                                <input type="hidden" name="id" id="id" value="{{$product->id}}">

                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input" name="image"
                                                       id="customFileLang">
                                                <label class="custom-file-label" for="customFileLang"
                                                       data-browse="Procurar">Selecione sua foto</label>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="col-md-8">


                                        <div class="form-group">
                                            <label for="nome">Nome</label>
                                            <input type="text" value="{{ $product->name }}" class="form-control"
                                                   name="nome" required id="nome"
                                                   aria-describedby="resturanteName" placeholder="Nome do seu producte">

                                        </div>

                                        <div class="input-group mb-2">

                                            <div class="input-group-prepend">
                                                <div class="input-group-text">R$</div>
                                            </div>
                                            <input type="text" class="form-control" value="" required name="valor"
                                                   id="valor" placeholder="Digite aqui o valor!">
                                        </div>


                                        <div class="form-group">
                                            <label for="desc">Descrição</label>
                                            <textarea class="form-control" name="desc"
                                                      id="desc">{{ $product->desc }}</textarea>
                                            <small id="resturanteName" class="form-text text-muted">Chegou a hora de
                                                falar um
                                                pouquinho sobre seu produto.
                                            </small>
                                        </div>


                                            <button type="submit" id="btnform" class="btn btn-primary">Cadastrar</button>



                                    </div>

                                </div>
                                </form>
                            </div>
                        </div>
                    </div>


                <div id="lista">


                </div>


            </div>
        </div>
    </div>

@endsection

@section('js')

    <script>


        $( "#lista" ).load("{{ route('product.lista',['id'=>$id]) }}")


        $('#formProduct').on('submit', function(event){

            event.preventDefault();
            $.ajax({
                url:"{{ route('product.store') }}",
                method:"POST",
                data:new FormData(this),
                dataType:'JSON',
                contentType: false,
                cache: false,
                processData: false,
                success:function(data)
                {

                    Swal.fire(
                        'Parabéns!',
                        'Produto cadastrado com sucesso!',
                        'success'
                    );

                   $( "#lista" ).load("{{ route('product.lista',['id'=>$id]) }}");

                }
            })
        });





    </script>

@endsection


