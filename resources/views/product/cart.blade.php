@extends('layouts.app')



@section('content')
    <div class="container mb-4">
        <div class="row">
            <div class="col-12">
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th scope="col"></th>
                            <th scope="col">Produto</th>
                            <th scope="col" class="text-center">Quantidade</th>
                            <th scope="col" class="text-right">Preço</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>

                        @if(session('cart'))

                            @foreach(session('cart') as $x => $id )
                                <tr>
                                    <td><img src="{{asset('storage/'.$id['image'])}}" width="50px"/></td>
                                    <td> {{ $id['name'] }}</td>

                                    <td><input class="form-control test" type="text" id="qtdItem" data-id="{{ $x }}" value=" {{ $id['qtd'] }} "/></td>

                                    <td class="text-right">R$ {{ $id['valor'] }} * {{ $id['qtd'] }} </td>
                                    <td class="text-right">
                                        <button class="retirar" data-id="{{ $x }}" class="btn btn-sm btn-danger">X</button>
                                    </td>

                                </tr>
                            @endforeach



                        @endif


                        <tr>

                            <td></td>
                            <td></td>
                            <td></td>
                            <td><strong>Total</strong></td>
                            <td class="text-right" id="totalValue">R$<strong ></strong></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="col mb-2">
                <div class="row">
                    <div class="col-sm-12  col-md-6">

                    </div>
                    <div class="col-sm-12 col-md-6 text-right">
                        <button class="btn btn-lg btn-block btn-success text-uppercase" id="finalizarPedido">Finalizar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

