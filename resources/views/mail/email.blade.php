
<html>
<head>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</head>
<body>

<div class="container mb-4">
    <div class="row">
        <div class="col-12">
            <h1>Pedido concluído com sucesso!</h1>
            <small>Verifique abaixo a lista dos itens comprados!</small>
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>

                    <tr>

                        <th scope="col">Produto</th>
                        <th scope="col" class="text-center">Quantidade</th>

                        <th></th>
                    </tr>
                    </thead>
                    <tbody>

                    @if(session('cart'))

                        @foreach(session('cart') as $x => $id )
                            <tr>

                                <td> {{ $id['name'] }}</td>

                                <td class="text-right">R$ {{ $id['valor'] }} * {{ $id['qtd'] }} </td>


                            </tr>
                        @endforeach



                    @endif


                    </tbody>
                </table>
            </div>
        </div>
        <div class="col mb-2">
            <div class="row">
                <div class="col-sm-12  col-md-6">
                    <h5>Agradeçemos a preferência</h5>
                </div>

            </div>
        </div>
    </div>
</div>

</body>

</html>
