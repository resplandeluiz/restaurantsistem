@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12" >
                @foreach($restaurants as $restaurant)
                    <div class="card mb-3">
                        <div class="row no-gutters">
                            <div class="col-md-4">

                                <img src="{{ asset('storage/'.$restaurant->image) }}" class="" width="100%" height="200px" alt="...">
                            </div>
                            <div class="col-md-8">
                                <div class="card-body">
                                    <h5 class="card-title">{{ $restaurant->name  }}</h5>
                                    <p class="card-text">{{ $restaurant->desc  }}</p>
                                    <p class="card-text">
                                        <small class="text-muted">Last updated 3 mins ago</small>
                                    </p>
                                    <a href="/restaurant/{{$restaurant->id}}/edit" class="btn btn-primary">Editar restaurante</a>
                                    <a href="/product/build/{{$restaurant->id}}" class="btn btn-primary">Cadastrar produtos</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach

            </div>
        </div>
    </div>
@endsection