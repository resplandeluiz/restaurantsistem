@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">

                    <div class="card-header">Cadastro Restaurante</div>

                        @if($restaurant->id == "")
                        {{ Form::open(array('route'=>'restaurant.store','method'=>'POST','id'=>'form','files' => 'true')) }}
                                @else
                                    {{ Form::model('', array('route' => array('restaurant.update', $restaurant->id), 'method' => 'PUT','files' => 'true')) }}
                                @endif

                                @csrf
                        <div class="card-body row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    @if( $restaurant->image )
                                        <img src="{{ asset('storage/'. $restaurant->image) }}" class="img-thumbnail rounded float-left" alt="...">
                                    @else
                                    <img src="{{ asset('img/restaurant-bg.jpg') }}" class="img-thumbnail rounded float-left" alt="...">
                                    @endif
                                <div class="custom-file">
                                    <input type="file"  class="custom-file-input" name="image" id="customFileLang">
                                    <label class="custom-file-label" for="customFileLang" data-browse="Procurar">Selecione sua foto</label>
                                </div>

                                </div>
                            </div>
                            <div class="col-md-8">


                                <div class="form-group">
                                    <label for="restaurantName">Nome</label>
                                    <input type="text" value="{{ $restaurant->name }}" class="form-control" name="name" required id="restaurantName"
                                           aria-describedby="resturanteName" placeholder="Nome do seu restaurante">

                                </div>

                                <div class="form-group">
                                    <label for="cnpj">CNPJ</label>
                                    <input type="text" class="form-control" value="{{ $restaurant->cnpj }}" required data-mask="00.000.000/0000-00" name="cnpj" id="cnpj" placeholder="CNPJ">
                                </div>

                                <div class="form-group">
                                    <label for="cnpj">Descrição</label>
                                    <textarea class="form-control" name="desc"   id="description">{{ $restaurant->desc }}</textarea>
                                    <small id="resturanteName" class="form-text text-muted">Chegou a hora de falar um
                                        pouquinho sobre seu negócio.
                                    </small>
                                </div>

                                @if($restaurant->id == "")
                                <button type="submit" class="btn btn-primary">Cadastrar</button>
                                    @else
                                    <button type="submit" class="btn btn-primary">Atualizar</button>
                                @endif


                            </div>

                        </div>
                    </form>
                </div>

            </div>

        </div>
    </div>



@endsection





