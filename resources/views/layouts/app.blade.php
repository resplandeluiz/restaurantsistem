<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Restaurant 24h/48h</title>


    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
<div id="app">
    <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
        <div class="container">
            <a class="navbar-brand" href="{{ url('/') }}">
                Restaurant 24h/48h
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false"
                    aria-label="{{ __('Toggle navigation') }}">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <!-- Left Side Of Navbar -->
                <ul class="navbar-nav mr-auto">

                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="navbar-nav ml-auto">
                    <!-- Authentication Links -->
                    @guest
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                        </li>
                        @if (Route::has('register'))
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                            </li>
                        @endif
                    @else

                        <li class="nav-item dropdown">


                            <div class="dropdown">
                                <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton"
                                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <img src="{{asset('img/shopping-cart.png')}}" width="25px">
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton" style="width:300px">


                                    <ul class="list-group">


                                        @if(session('cart'))
                                            @foreach(session('cart') as $id )

                                                <li class="list-group-item d-flex justify-content-between align-items-center">
                                                    {{ $id['name'] }}
                                                    <span class="badge badge-primary badge-pill">  {{ $id['qtd'] }}</span>


                                                </li>
                                            @endforeach
                                                <li class="list-group-item d-flex justify-content-between align-items-center">
                                                    <a href="/cart">Finalizar compra</a>


                                                </li>


                                        @endif


                                    </ul>
                                </div>
                            </div>
                        </li>









                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                Restaurantes <span class="caret"></span>
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a href="/restaurant" class="dropdown-item">
                                    Meus restaurantes
                                </a>

                                <a href="/restaurant/create" class="dropdown-item">
                                    Cadastrar restaurantes
                                </a>


                            </div>
                        </li>



                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>

                                <a class="dropdown-item" href="/user/edit" >
                                  Editar Perfil
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                      style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>








                    @endguest
                </ul>
            </div>
        </div>
    </nav>

    <main class="py-4">
        <div class="container">

            @if(Session::has('message'))
                <p class="alert alert-success mt-3">{{ Session::get('message') }}</p>
            @endif

            @if(Session::has('error'))
                <p class="alert alert-danger mt-3">{{ Session::get('error') }}</p>
            @endif
        </div>
        @yield('content')
    </main>
</div>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js" type="text/javascript"></script>
<script src="{{ asset('js/jquery-3.4.1.min.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<script src="{{ asset('js/jquery.mask.js') }}"></script>

<script>

    $(document).ready(function () {
        UpdateValue();
        $('#cnpj').mask('00.000.000/0000-00');

        $('#valor').mask("#.##0,00", {reverse: true});


        var quantitiy = 0;
        $('.quantity-right-plus').click(function (e) {

            // Stop acting like a button
            e.preventDefault();
            // Get the field name
            var quantity = parseInt($('#quantity').val());

            // If is not undefined

            $('#quantity').val(quantity + 1);


            // Increment

        });

        $('.quantity-left-minus').click(function (e) {
            // Stop acting like a button
            e.preventDefault();
            // Get the field name
            var quantity = parseInt($('#quantity').val());

            // If is not undefined

            // Increment
            if (quantity > 0) {
                $('#quantity').val(quantity - 1);
            }
        });


    });


    $('.test').on('change', function() {

        var id = $(this).data("id");
        var token = $("meta[name='csrf-token']").attr("content");

        $.ajax(
            {
                url: "/cartUpdateItem/" + id,
                method: 'post',
                data: {
                    "id": id,
                    "_token": token,
                    "qtd": $(this).val(),
                },
                success: function (data) {

                    Swal.fire(
                        'Parabéns!',
                        'Produto excluído com sucesso!',
                        'success'
                    );
                    UpdateValue();

                },
                error: function (error) {
                    console.log(error)
                }
            });


    });

    $('.retirar').click(function(){

        var id = $(this).data("id");
        var token = $("meta[name='csrf-token']").attr("content");

        $.ajax(
            {
                url: "/removeCartItem/" + id,
                method: 'delete',
                data: {
                    "id": id,
                    "_token": token,
                },
                success: function (data) {

                    Swal.fire(
                        'Parabéns!',
                        'Produto excluído com sucesso!',
                        'success'
                    );
                    UpdateValue();

                },
                error: function (error) {
                    console.log(error)
                }
            });

    });


    $('#carrinho').click(function(){


        UpdateValue();

    });
    function UpdateValue(){
        $.ajax(
            {
                url: "/cartValue/",
                method: 'get',
                success: function (data) {
                    $('#totalValue').children().html(data)
                }
            });
    }


    $('#finalizarPedido').click(function(){
        var token = $("meta[name='csrf-token']").attr("content");
        $.ajax(
            {
                url: "/sendEmail",
                method: 'post',
                data: {
                    "_token": token,
                },
                success: function (data) {
                    window.location.replace("/pedidos");
                }
            });

    })
    
    
    
    $('#updateUser').click(function () {
        var token = $("meta[name='csrf-token']").attr("content");
        $.ajax(
            {
                url: "/updateUser",
                method: 'post',
                data: {
                    "_token": token,
                    'name': $('#name').val(),
                    'email': $('#email').val()
                },
                success: function (data) {
                    Swal.fire(
                        'Parabéns!',
                        'Usuário atualizado com sucesso!',
                        'success'
                    );

                    $('#name').val(data.name)
                    $('#email').val(data.email)
                }
            });
        
    })
</script>

@yield('js')

</body>
</html>
