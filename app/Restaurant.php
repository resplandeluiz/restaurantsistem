<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Restaurant extends Model
{
    protected $fillable = ['name','cnpj','desc'];
    protected $table = 'restaurants';
}
