<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = ['nome','valor','desc','restaurants_id'];
    protected $table = 'products';
}
