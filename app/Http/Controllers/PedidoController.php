<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pedido;
use App\PedidoProduto;
use Illuminate\Support\Facades\DB;

class PedidoController extends Controller
{


    public function index()
    {


        $pedidos = DB::table('pedidos')
            ->join('pedidos_products', 'pedidos_products.pedido_id', '=', 'pedidos.id')
            ->join('products', 'products.id', '=', 'pedidos_products.produto_id')
            ->where('pedidos.user_id', '=', auth()->user()->id)->get();



        foreach($pedidos as $pedido){

            $resultado[$pedido->pedido_id][] = $pedido;

        }


        return view('pedidos.index', compact('resultado'));

    }

    public static function create()
    {


        $pedido = new Pedido();
        $pedido->user_id = auth()->user()->id;
        $pedido->save();

        foreach (session()->get('cart') as $index => $cart) {

            $pedidoProduto = new PedidoProduto();
            $pedidoProduto->pedido_id = $pedido->id;
            $pedidoProduto->produto_id = $index;
            $pedidoProduto->valor_produto = $cart['valor'];
            $pedidoProduto->qtd_produto = $cart['qtd'];
            $pedidoProduto->save();
        }

        return $pedido;


    }
}
