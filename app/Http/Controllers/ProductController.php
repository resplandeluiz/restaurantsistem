<?php

namespace App\Http\Controllers;



use Illuminate\Http\Request;
use Session;
use Redirect;

use App\Product;
use App\Restaurant;

use App\Http\Controllers\UploadFile;



class ProductController extends Controller
{
    public function addToCart($id){


        $product = Product::find($id);

        if(!$product) {

            abort(404);

        }

        $cart = session()->get('cart');


        if(!$cart) {

            $cart = [
                $id => [
                    "name" => $product->nome,
                    "qtd" => 1,
                    "desc" => $product->desc,
                    "valor" => $product->valor,
                    "image" => $product->image
                ]
            ];

            session()->put('cart', $cart);
            Session::flash('message','Product adicionado ao carrinho com sucesso!');
            return redirect()->back();
        }


        if(isset($cart[$id])) {

            $cart[$id]['qtd']++;

            session()->put('cart', $cart);
            Session::flash('message','Product adicionado ao carrinho com sucesso!');
            return redirect()->back();

        }

        // if item not exist in cart then add to cart with quantity = 1
        $cart[$id] = [
            "name" => $product->nome,
            "qtd" => 1,
            "desc" => $product->desc,
            "valor" => $product->valor,
            "image" => $product->image
        ];

        session()->put('cart', $cart);

        return redirect()->back()->with('success', 'Product added to cart successfully!');
    }

    public function updateCart(Request $request)
    {
        if($request->id and $request->quantity)
        {
            $cart = session()->get('cart');

            $cart[$request->id]["qtd"] = $request->quantity;

            session()->put('cart', $cart);

            session()->flash('success', 'Cart updated successfully');
        }
    }

    public function removeItemCart(Request $request)
    {
        if($request->id) {

            $cart = session()->get('cart');

            if(isset($cart[$request->id])) {

                unset($cart[$request->id]);

                session()->put('cart', $cart);
            }

            session()->flash('success', 'Product removed successfully');
        }
    }




    public function getProduct($id){

        return Product::find($id);

    }

    public function lista($id)
    {
        $products = Product::where('restaurants_id',$id)->orderBy('id','desc')->paginate(3);

        return view('product.lista', compact('products'));
    }

    public function index()
    {
        self::create();

        return view('product.index', compact('products','product'));
    }

    public function create($id)    {


        $products = Product::where('restaurants_id',$id)->get();
        $product = new Product();
        return view('product.index', compact('product', 'products','id'));
    }

    public function store(Request $request)
    {

        if($request->id){
            $product = Product::find($request->id);
        }else{
            $product = new Product();
        }

        $product->fill($request->all());


        if ($request->file('image')) {

            if ($product->image) {
                UploadFile::deletedFile($product->image);
            }

            $product->image = UploadFile::upload($request->file('image'), 'IMG');
        }

        $product->save();
        return $request->restaurants_id;
    }


    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $product = Product::find($id);
        $products = Product::all();

        if($product == null){


        }




        return view('product.index', compact('product', 'products'));
    }


    public function update(Request $request, $id)
    {

        $product = Product::find($id);

        $product->fill($request->all());

        if ($request->file('image')) {

            if ($product->image) {
                UploadFile::deletedFile($product->image);
            }

            $product->image = UploadFile::upload($request->file('image'), 'IMG');
        }

        $product->save();


        Session::flash('message', 'Atualizado com sucesso');
        return Redirect::to('product');
    }


    public function destroy($id)
    {
        Product::find($id)->delete();
        return ['true'];
    }
}