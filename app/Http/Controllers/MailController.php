<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\PedidoController;

class MailController extends Controller
{
    public function send()
    {


        PedidoController::create();

        $to_name = 'Restaurante do Luiz';
        $to_email = auth()->user()->email;
        $data = array('name'=>"Sam Jose", "body" => "Test mail");

        Mail::send('mail.email', $data, function($message) use ($to_name, $to_email) {
            $message->to($to_email, $to_name)
                ->subject(' Pedido#, concluído com sucesso!');
            $message->from('xptorestaurante2019@gmail.com','RestaurantSistem');
        });

        session()->forget('cart');




    }
}
