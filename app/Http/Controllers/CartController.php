<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CartController extends Controller
{
    public function index()
    {

        return view('product.cart');


    }

    public function updateCart(Request $request)
    {


        if ($request->id and $request->qtd) {
            $cart = session()->get('cart');

            $cart[$request->id]["qtd"] = $request->qtd;

            session()->put('cart', $cart);

            session()->flash('success', 'Cart updated successfully');
        }
    }


    public function removeItemCart(Request $request)
    {
        if ($request->id) {

            $cart = session()->get('cart');

            if (isset($cart[$request->id])) {

                unset($cart[$request->id]);

                session()->put('cart', $cart);
            }

            session()->flash('success', 'Product removed successfully');
        }
    }

        public function getValues(){
            $cartItems = session('cart');
            $valorTotal = 0;
            foreach($cartItems as $item){
                $valorTotal += floatval($item ['valor'])* $item['qtd'];
            }

            return $valorTotal;
    }


}
