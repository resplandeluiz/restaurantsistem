<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Restaurant;
use Session;
use Redirect;
use App\Http\Controllers\UploadFile;


class RestaurantController extends Controller
{

    public function index()
    {

        $restaurants = Restaurant::where('user_id', auth()->user()->id)->get();
        return view('restaurant.index', compact('restaurants'));
    }

    public function create()
    {

        $restaurants = Restaurant::all();
        $restaurant = new Restaurant();

        return view('restaurant.create', compact('restaurant', 'restaurants'));
    }

    public function store(Request $request)
    {

        $restaurant = new Restaurant();
        $restaurant->fill($request->all());
        $restaurant->user_id = auth()->user()->id;
        if ($request->file('image')) {

            if ($restaurant->image) {
                UploadFile::deletedFile($restaurant->image);
            }

            $restaurant->image = UploadFile::upload($request->file('image'), 'IMG');
        }

        $restaurant->save();

        Session::flash('message', 'Cadastrado com sucesso');
        return Redirect::to('restaurant');
    }


    public function show($id)
    {
        //
    }

    public function edit($id)
    {

        $restaurants = Restaurant::all();
        $restaurant =  Restaurant::find($id);

        return view('restaurant.create', compact('restaurant', 'restaurants'));
    }


    public function update(Request $request, $id)
    {

        $restaurant = Restaurant::find($id);

        $restaurant->fill($request->all());

        if ($request->file('image')) {

            if ($restaurant->image) {
                UploadFile::deletedFile($restaurant->image);
            }

            $restaurant->image = UploadFile::upload($request->file('image'), 'IMG');
        }

        $restaurant->save();


        Session::flash('message', 'Atualizado com sucesso');
        return Redirect::to('restaurant');
    }


    public function destroy($id)
    {

        $restaurant = Restaurant::find($id)->delete();


        Session::flash('message', 'Exlcuído com sucesso!');
        return Redirect::to('restaurant');
    }
}