<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;

class Treatment extends Controller
{
    public static function getToSelect($array)
    {
        if(!isset($array) || $array == null || $array == ""){
            return false;
        }
        foreach ($array as $a) {
            $array_select[$a->id] = $a->name;
        }

        return $array_select;


    }


    public static function makeValidationForm($request, $validationRules)
    {

        $validator = Validator::make($request, $validationRules);

        if ($validator->fails()) {

            $msg['status'] = false;
            $msg['message'] = $validator->messages()->toJson();

        } else {

            $msg['status'] = true;
        }

        return $msg;
    }


}
