<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;

class UploadFile extends Controller
{

    public static function upload($file,$constant){

       if (is_array($file)){

           $file = reset($file);

       }

        if(self::validatedFile($file->getClientOriginalExtension(),$constant)){

            $file->store('/');

            if($file->getError() > 0 ){

                return ['msg'=>'Erro ao tentar realizar a operação'];

            }
            return $file->hashName();
        }

        return false;

    }


    public static function deletedFile($hashName){

        return Storage::delete($hashName);

    }

    public static function validatedFile($currentExtension,$constant){

        $typefile = [ 'IMG'=>['png','bmp','jpg','jpeg'],'VIDEO'=>['mp4','avi']];


        foreach($typefile[$constant] as $type){

            if($type == $currentExtension){
                return true;
            }
        }

        return false;

    }



}
