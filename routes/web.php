<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::group(['middleware'=>['auth']],function(){

Route::get('/', function () {
    return redirect('home');
});



Route::get('/home', 'HomeController@index')->name('home');

Route::resource('restaurant', 'RestaurantController');

//Route::get('product/create/{id}', 'ProductController@create');
//Route::resource('product', 'ProductController');


Route::get('product/build/{id}', ['as' => 'product.create', 'uses' => 'ProductController@create']);
Route::get('product/lista/{id}', 'ProductController@lista')->name('product.lista');
Route::get('product/getProduct/{id}', 'ProductController@getProduct')->name('product.get');
Route::get('product/addCart/{id}', 'ProductController@addToCart');

Route::get('user/edit','UserController@create');
Route::post('updateUser','UserController@store');


Route::get('cart','CartController@index');
Route::get('cartValue','CartController@getValues');
Route::post('cartUpdateItem/{id}','CartController@updateCart');
Route::delete('removeCartItem/{id}','CartController@removeItemCart');

Route::get('pedidos','PedidoController@index');

Route::post('sendEmail','MailController@send');

Route::resource('product', 'ProductController', ['names' => [
    'create' => 'product.build'
]]);

});